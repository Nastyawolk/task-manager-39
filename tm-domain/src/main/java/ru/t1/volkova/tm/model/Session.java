package ru.t1.volkova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.enumerated.Role;

import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public final class Session extends AbstractUserOwnedModel {

    @NotNull
    private Date date = new Date();

    @Nullable
    private Role role = null;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Session session = (Session) o;
        return session.getId().equals(((Session) o).getId()) && session.getUserId().equals(((Session) o).getUserId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDate(), getRole());
    }
}
