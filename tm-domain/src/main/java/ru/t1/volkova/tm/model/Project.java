package ru.t1.volkova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.model.IWBS;
import ru.t1.volkova.tm.enumerated.Status;

import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractUserOwnedModel implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    private Date created = new Date();

    public Project(@NotNull final String name, @NotNull final Status status) {
        this.name = name;
        this.status = status;
    }

    @Override
    public String toString() {
        return getName() + " | " +
                getId() + " | " +
                getDescription() + " | ";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return name.equals(project.name) &&
                description.equals(project.description) &&
                status.getDisplayname().equals(project.status.getDisplayname()) &&
                getId().equals(project.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, status, created);
    }


}
