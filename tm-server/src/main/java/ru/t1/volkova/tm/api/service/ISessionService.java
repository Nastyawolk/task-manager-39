package ru.t1.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.model.Session;

import java.sql.SQLException;
import java.util.List;

public interface ISessionService {

    @NotNull
    Session add(@NotNull Session session) throws SQLException;

    @Nullable
    Session removeOne(@NotNull Session session) throws SQLException;

    @Nullable
    Session findOneById(@Nullable String userId, @Nullable String id) throws SQLException;

    @NotNull
    Session findOneByIndex(@Nullable String userId, @NotNull Integer index) throws SQLException;

    int getSize(@Nullable String userId) throws SQLException;

    @Nullable
    List<Session> findAll(@Nullable String userId) throws SQLException;

    @Nullable
    Session removeOneById(@Nullable String userId, @Nullable String id) throws SQLException;

    @Nullable
    Session removeOneByIndex(@Nullable String userId, @NotNull Integer index) throws SQLException;

    void removeAll(@Nullable String userId) throws SQLException;

    @NotNull
    Boolean existsById(@Nullable String userId, @Nullable String id) throws SQLException;

}
