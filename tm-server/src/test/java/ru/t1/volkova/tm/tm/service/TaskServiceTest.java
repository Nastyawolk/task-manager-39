package ru.t1.volkova.tm.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.api.service.IProjectService;
import ru.t1.volkova.tm.api.service.ITaskService;
import ru.t1.volkova.tm.api.service.IUserService;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.exception.entity.TaskNotFoundException;
import ru.t1.volkova.tm.exception.field.*;
import ru.t1.volkova.tm.model.Task;
import ru.t1.volkova.tm.model.User;
import ru.t1.volkova.tm.service.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Random;

public class TaskServiceTest {

    private static String USER_ID;

    private static String USER_TEST_ID;

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Nullable
    private List<Task> taskList;

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, projectService, taskService, propertyService);

    @Before
    public void initRepository() throws SQLException {
        taskList = taskService.findAlls();
        @Nullable final User user = userService.findByLogin("user");
        @Nullable final User userTest = userService.findByLogin("test");
        if (user == null || userTest == null || taskList == null) return;
        USER_ID = user.getId();
        USER_TEST_ID = userTest.getId();
    }

    @Test
    public void testCreate(
    ) throws Exception {
        int size = taskService.getSize(USER_ID);
        @NotNull final Task task = taskService.create(USER_ID, "new_task", "new description");
        Assert.assertEquals(task, taskService.findOneById(USER_ID, task.getId()));
        Assert.assertEquals(size + 1, taskService.getSize(USER_ID));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateUserId(
    ) throws SQLException {
        taskService.create(null, "new_task", "new description");
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateNameEmpty(
    ) throws SQLException {
        taskService.create(USER_ID, null, "new description");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testCreateDescriptionEmpty(
    ) throws SQLException {
        taskService.create(USER_TEST_ID, "new", null);
    }

    @Test
    public void testUpdateById() throws SQLException {
        @NotNull final String newName = "new taskUpd";
        @NotNull final String newDescription = "new taskUpd";
        if (taskList == null) return;
        @Nullable final Task task = taskList.get(0);
        if (task == null) return;
        taskService.updateById(task.getUserId(), task.getId(), newName, newDescription);
        @Nullable final Task taskUpd = taskService.findOneById(task.getUserId(), task.getId());
        if (taskUpd == null) return;
        Assert.assertEquals(newName, taskUpd.getName());
        Assert.assertEquals(newDescription, taskUpd.getDescription());
    }

    @Test(expected = TaskNotFoundException.class)
    public void testUpdateNotFoundTask(
    ) throws SQLException {
        @NotNull final String newName = "new task";
        @NotNull final String newDescription = "new task";
        @NotNull final String id = "non-existent-id";
        taskService.updateById(USER_ID, id, newName, newDescription);
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateIdEmpty(
    ) throws SQLException {
        taskService.updateById(USER_ID, null, "name", "new description");
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateNameEmpty(
    ) throws SQLException {
        if (taskList == null) return;
        taskService.updateById(taskList.get(0).getUserId(), taskList.get(0).getId(), null, "new description");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testUpdateDescriptionEmpty(
    ) throws SQLException {
        if (taskList == null) return;
        taskService.updateById(taskList.get(0).getUserId(), taskList.get(0).getId(), "name", null);
    }

    @Test
    public void testUpdateByIndex() throws SQLException {
        @NotNull final String newName = "new nameUpd";
        @NotNull final String newDescription = "new descUpd";
        @NotNull final Random random = new Random();
        final int index = random.nextInt(taskService.getSize(USER_ID));
        taskService.updateByIndex(USER_ID, index, newName, newDescription);
        @Nullable final Task task = taskService.findOneByIndex(USER_ID, index);
        Assert.assertEquals(newName, task.getName());
        Assert.assertEquals(newDescription, task.getDescription());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateIdEmptyByIndex(
    ) throws SQLException {
        taskService.updateByIndex(USER_ID, 10, "name", "new description");
        taskService.updateByIndex(USER_ID, null, "name", "new description");
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateNameEmptyByIndex(
    ) throws SQLException {
        @NotNull final Random random = new Random();
        final int index = random.nextInt(taskService.getSize(USER_ID));
        taskService.updateByIndex(USER_ID, index, null, "new description");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testUpdateDescriptionEmptyByIndex(
    ) throws SQLException {
        @NotNull final Random random = new Random();
        final int index = random.nextInt(taskService.getSize(USER_ID));
        taskService.updateByIndex(USER_TEST_ID, index, "name", null);
    }

    @Test
    public void testChangeStatusById() throws SQLException {
        if (taskList == null) return;
        @NotNull final String id = taskList.get(0).getId();
        @NotNull final String userId = taskList.get(0).getUserId();
        @Nullable final Task task = taskService.changeTaskStatusById(userId, id, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeStatusIdEmptyById() throws SQLException {
        taskService.changeTaskStatusById(USER_ID, null, Status.IN_PROGRESS);
        taskService.changeTaskStatusById(USER_ID, "", Status.IN_PROGRESS);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testChangeStatusTaskNotFoundById() throws SQLException {
        taskService.changeTaskStatusById("USER_ID_12", "id", Status.IN_PROGRESS);
        taskService.changeTaskStatusById(USER_ID, "non-existent", Status.IN_PROGRESS);
    }

    @Test
    public void testChangeStatusByIndex() throws SQLException {
        @NotNull final Random random = new Random();
        final int index = random.nextInt(taskService.getSize(USER_ID));
        @Nullable final Task task = taskService.changeTaskStatusByIndex(USER_ID, index, Status.IN_PROGRESS);
        if (task == null) return;
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeStatusIndexExceptionByIndex() throws SQLException {
        taskService.changeTaskStatusByIndex(USER_ID, 22, Status.IN_PROGRESS);
        taskService.changeTaskStatusByIndex(USER_ID, null, Status.IN_PROGRESS);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testChangeStatusTaskNotFoundByIndex() throws SQLException {
        taskService.changeTaskStatusById("USER_ID_12", "id", Status.IN_PROGRESS);
        taskService.changeTaskStatusById(USER_ID, "non-existent", Status.IN_PROGRESS);
    }

    @Test
    public void testFindOneById() throws SQLException {
        @NotNull final Random random = new Random();
        final int index = random.nextInt(taskService.getSize(USER_ID));
        if (taskList == null) return;
        Assert.assertNotNull(taskService.findOneById(USER_ID, taskList.get(index).getId()));
    }

    @Test(expected = TaskNotFoundException.class)
    public void testFindOneByIdNegative() throws SQLException {
        Assert.assertNotNull(taskService.findOneById(USER_ID, "non-existent"));
    }

    @Test
    public void testFindAllByProjectId() throws Exception {
        if (taskList == null) return;
        @Nullable String projectId = null;
        @Nullable String userId = null;
        for (@NotNull final Task task : taskList) {
            if (task.getProjectId() == null) continue;
            projectId = task.getProjectId();
            userId = task.getUserId();
            break;
        }
        @Nullable final List<Task> tasks = taskService.findAllByProjectId(userId, projectId);
        Assert.assertNotNull(tasks);
    }

    @Test
    public void findAllByProjectIdNegative() throws Exception {
        if (taskList == null) return;
        @Nullable String userId = null;
        for (@NotNull final Task task : taskList) {
            userId = task.getUserId();
            break;
        }
        @Nullable final List<Task> tasks = taskService.findAllByProjectId(userId, "non-existent");
        if (tasks == null) return;
        Assert.assertEquals(0, tasks.size());
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void findAllByProjectIdProjectNull() throws SQLException {
        taskService.findAllByProjectId(USER_ID, null);
        taskService.findAllByProjectId(USER_ID, "");
    }

}
